use clap::{arg, Arg, Command};

pub fn cli() -> Command {
    Command::new("cutlet")
        .about("simple template-based snippet manager")
        .version("0.1")
        .subcommand_required(true)
        .arg_required_else_help(true)
        .author("slippery nickels")
        .subcommand(
            Command::new("add")
                .about("Add new snippet")
                .arg(arg!(<name> "Name of the snippet to create"))
                .arg_required_else_help(true),
        )
        .subcommand(
            Command::new("list")
                .about("List all snippets")
                .arg(arg!([name] "Name of the snippet to list template variables")),
        )
        .subcommand(
            Command::new("edit")
                .about("Edit an existing snippet")
                .arg(arg!(<name> "Name of the snippet to edit"))
                .arg_required_else_help(true),
        )
        .subcommand(
            Command::new("get")
                .about("Get a snippet")
                .arg(arg!(<name> "Name of the snippet"))
                .arg(
                    Arg::new("values")
                        .help("Template values to apply (if applicable)")
                        .num_args(0..),
                ),
        )
        .subcommand(
            Command::new("delete")
                .about("Delete a snippet")
                .arg(arg!(<name> "Name of the snippet"))
                .arg_required_else_help(true),
        )
}
