use directories::BaseDirs;
use std::path::{Path, PathBuf};
use unqlite::{UnQLite, KV};

pub fn get_own_file(filename: &str) -> PathBuf {
    let base_dirs = BaseDirs::new().expect("could not get base dirs");
    Path::new(base_dirs.config_dir())
        .join("cutlet")
        .join(filename)
}

pub fn get_db() -> UnQLite {
    UnQLite::create(get_own_file("snippets.db").to_str().unwrap())
}

pub fn get_snippet(key: &String) -> String {
    let db = get_db();
    let snippet_bytes = match db.kv_fetch(key) {
        Ok(sb) => sb,
        Err(err) => panic!("Could not get snippet \"{}\": {}", key, err),
    };
    match String::from_utf8(snippet_bytes) {
        Ok(sc) => sc,
        Err(e) => panic!("Invalid UTF-8 sequence: {}", e),
    }
}
