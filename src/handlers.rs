use crate::{
    template::{find_tags, render},
    util::{get_db, get_snippet},
};
use clap::ArgMatches;
use unqlite::{Cursor, KV};

pub fn add_handler(sub_matches: &ArgMatches) {
    let snippet_name = sub_matches
        .get_one::<String>("name")
        .expect("snippet name required");

    let snippet_contents = edit::edit("").expect("new snippet");

    let db = get_db();
    db.kv_store(snippet_name, snippet_contents.clone()).unwrap();

    println!("New snippet named \"{}\":", snippet_name);
    println!("{}", snippet_contents);
}

pub fn list_handler(sub_matches: &ArgMatches) {
    let db = get_db();

    let snippet_name = sub_matches.get_one::<String>("name");

    match snippet_name {
        Some(name) => {
            if db.kv_contains(name) {
                let snippet_contents = get_snippet(name);
                let tag_names = find_tags(snippet_contents);
                println!("Tag names for snippet \"{}\":", name);
                println!("{}", tag_names.join(", "));
            } else {
                println!("No snippet named \"{}\".", name);
            }
        }
        None => {
            println!("Available snippets:");
            let mut entry = db.first();
            loop {
                if entry.is_none() {
                    break;
                }
                let record = entry.expect("valid entry");
                let key = match String::from_utf8(record.key()) {
                    Ok(k) => k,
                    Err(e) => panic!("Invalid UTF-8 sequence: {}", e),
                };
                println!("  * {} ({} bytes)", key, record.value().len());
                entry = record.next();
            }
        }
    }
}

pub fn edit_handler(sub_matches: &ArgMatches) {
    let snippet_name = sub_matches
        .get_one::<String>("name")
        .expect("snippet name required");

    let old_snippet_contents = get_snippet(snippet_name);
    let new_snippet_contents = edit::edit(old_snippet_contents).expect("updated snippet");

    let db = get_db();
    db.kv_store(snippet_name, new_snippet_contents).unwrap();

    println!("Updated snippet \"{}\".", snippet_name);
}

pub fn get_handler(sub_matches: &ArgMatches) {
    let snippet_name = sub_matches.get_one::<String>("name").expect("required");

    let db = get_db();
    if !db.kv_contains(snippet_name) {
        println!("No snippet found by the name \"{}\".", snippet_name);
        return;
    }

    let snippet_contents = get_snippet(snippet_name);

    if let Some(vals) = sub_matches.get_many::<String>("values") {
        let template_values = vals.map(|s| s.to_string()).collect::<Vec<_>>();
        println!("{}", render(snippet_contents, template_values));
    } else {
        println!("Snippet \"{}\":", snippet_name);
        println!("{}", snippet_contents);
    }
}

pub fn delete_handler(sub_matches: &ArgMatches) {
    let snippet_name = sub_matches.get_one::<String>("name").expect("required");

    let db = get_db();
    if !db.kv_contains(snippet_name) {
        println!("No snippet found by the name \"{}\".", snippet_name);
        return;
    }

    db.kv_delete(snippet_name).unwrap();
    println!("Deleted snippet \"{}\".", snippet_name);
}
