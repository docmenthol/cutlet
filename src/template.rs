use itertools::Itertools;
use regex::Regex;

pub fn find_tags(template: String) -> Vec<String> {
    let re = Regex::new(r"\{([\w-]+)\}").unwrap();
    let names: Vec<String> = re
        .captures_iter(template.as_str())
        .map(|c| format!("{}", &c[1]))
        .collect();
    names.iter().unique().map(|n| n.into()).collect()
}

pub fn render(template: String, values: Vec<String>) -> String {
    let mut snippet = template.clone();
    let tag_names = find_tags(template);
    let search_pairs = tag_names.iter().zip(values.iter());

    for (tag, val) in search_pairs {
        let search = format!("\\{{{}\\}}", tag);
        let re = Regex::new(search.as_str()).unwrap();
        snippet = re.replace_all(snippet.as_str(), val).to_string();
    }

    snippet.to_string()
}
