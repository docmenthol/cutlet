mod cli;
mod handlers;
mod template;
mod util;

use crate::{
    cli::cli,
    handlers::{add_handler, delete_handler, edit_handler, get_handler, list_handler},
    util::get_own_file,
};
use std::fs;

fn main() -> Result<(), String> {
    if !get_own_file("").exists() {
        fs::create_dir(get_own_file("")).ok();
    }

    let matches = cli().get_matches();

    match matches.subcommand() {
        Some(("add", sub_matches)) => add_handler(sub_matches),
        Some(("list", sub_matches)) => list_handler(sub_matches),
        Some(("edit", sub_matches)) => edit_handler(sub_matches),
        Some(("get", sub_matches)) => get_handler(sub_matches),
        Some(("delete", sub_matches)) => delete_handler(sub_matches),
        _ => unreachable!(),
    }

    Ok(())
}
