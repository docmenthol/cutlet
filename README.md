# cutlet

A simple stdout-based terminal snippet manager for editors like helix and vim.

Snippets use a very rudimentary, Handlebars-like syntax. There are only {tags},
and like tags get replaced with the same value. Here's an example:

```
$ cutlet get demo
Snippet "demo":
Hello, {name}! This is a {day} day. Anyway, goodbye {name}!
$ cutlet get demo world good
Hello, world! This is a good day. Anyway, goodbye world!
```

Everything else should be relatively self-explanitory. Adding/editing is done
using your default editor. If you want a different one, set your EDITOR environment
variable to something else.
